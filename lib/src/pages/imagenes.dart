import 'package:flutter/material.dart';

class Imagenes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('imagenes',),
          centerTitle: true,
        ),
        body: ListView(
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(40.0),
              child: Image.network("https://miracomosehace.com/wp-content/uploads/2020/05/id_3995.jpg"),
            ),
             Container(
                  padding: EdgeInsets.all(40.0),
              child: Image.network("https://www.skinatech.com/portal/wp-content/uploads/2017/05/original.png"),
            ),
            Container(
              padding: EdgeInsets.all(40.0),
              child: Image.network("https://miracomosehace.com/wp-content/uploads/2020/05/id_3995.jpg"),
            ),
            Container(
              padding: EdgeInsets.all(40.0),
              child: Image.network("https://www.skinatech.com/portal/wp-content/uploads/2017/05/original.png"),
            ),
            Container(
              padding: EdgeInsets.all(40.0),
              child: Image.network("https://www.skinatech.com/portal/wp-content/uploads/2017/05/original.png"),)
          ]
        ),
      ),
    );
  }
}